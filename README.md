# README #

Widget Helper for Corona SDK

Version: 0.4

A collection of functions that spices up the use of UI elements in Corona SDK. Currently a work in progress.

Requirements:
Corona Premium Graphics (Graphics 2.0) for the toggle buttons (it uses fills). 

Usage:

local widgetHelper = require("WidgetHelper")
