------------------------------------------------
--
-- WidgetHelper class
-- Author: Geo Dela Paz
-- https://about.me/iamgeodelapaz
-- https://twitter.com/iamgeodelapaz
-- 
-- Set of functions that sort of extends and eases
-- the creation of widgets on the Corona App
--
------------------------------------------------

local WidgetHelper = {}

------------------------
-- Requires
local widget = require( "widget" )

------------------------
-- Variables, Forward-declarations

-- Private

-- Public
local createButton
local createToggleButton
local checkToggleButtonStates

------------------------------------------------
-- Private functions

------------------------------------------------
-- Public functions

------------------------------------------------
-- Returns a button widget
--
-- @param   Table           params  Expected parameters for the button widget
--                                  (see Corona SDK API)
-- @param   int             x       The X-coordinate, defaults to 0
-- @param   int             y       The Y-coordinate, defaults to 0
-- @param   DisplayGroup    group   The display group, pass nil if none.
-- @param   float           anchorX Anchor or coordinate of reference for X.
--                                  Defaults to 0.5
-- @param   float           anchorY Anchor or coordinate of reference for Y.
--                          Defaults to 0.5
--
-- @return  DisplayObject   The button widget
createButton = function( params, x, y, group, anchorX, anchorY )
	local button = widget.newButton(
		params
	)
    button.anchorX, button.anchorY = anchorX or 0.5, anchorY or 0.5
    button.x, button.y = x or 0, y or 0

    if group then
        group:insert( button )
    end

    return button;
end
WidgetHelper.createButton = createButton

------------------------------------------------
-- Returns a toggle button widget that uses swaps
-- for showing button state.
--
-- @todo: implement Event-handling for cleaner code
-- http://www.coronalabs.com/blog/2013/11/26/tutorial-techniques-for-swapping-images/
--
-- @param   int             width       The widget width
-- @param   int             height      The widget height
-- @param   int             overWidth   The widget's alternate width
-- @param   int             overWeight  The widget's alternate height
-- @param   mixed           default     The widget's unpressed image,
--                                      can be a string path of the resource
--                                      for image fill, or a table containing 
--                                      paths of images for a composite fill.
-- @param   mixed           over        The widget's pressed image,
--                                      can be a string path of the resource
--                                      for image fill, or a table containing 
--                                      paths of images for a composite fill.
-- @param   function        turnOn      The callback if button is toggled on
-- @param   function        turnOff     The callback if button is toggled off
-- @param   int             x           The X-coordinate, defaults to 0
-- @param   int             y           The Y-coordinate, defaults to 0
-- @param   DisplayGroup    group       The display group, pass nil if none.
--
-- @return  customDisplayObject   The button widget
createToggleButton = function( params )
    local width         = params.width
    local height        = params.height
    local overWidth     = params.overWidth
    local overHeight    = params.overHeight
    local default   = params.default
    local over      = params.over
    local turnOn    = params.turnOn
    local turnOff   = params.turnOff
    local x = params.x
    local y = params.y
    local xOver = params.xOver or x
    local yOver = params.yOver or y
    local group = params.group
    local anchorX = params.anchorX or 0.5
    local anchorY = params.anchorY or 0.5
    
    local listener
    
    local button = display.newRect( 0, 0, width, height )
    --button.touch = listener
    
    function button:callback()
        if self.notPressed then
            return self.turnOn()
        else
            return self.turnOff()
        end
    end

    ---------------------------
    -- Toggles the button state
    -- pass true to execute callback
    function button:toggle( performCallback )
        
        local _callbackSuccess
        local eff = "composite.add"

        -- Note: named the state flag "notPressed" for coherency reasons
        if performCallback then
            _callbackSuccess = self:callback()
        else
            _callbackSuccess = true
        end

        if not _callbackSuccess then
            return true;
        end
        
        if self.notPressed then
            self.fill = self.images.over
            self.notPressed = false

            if self.images.over.type == "composite" then self.fill.effect = eff; end
            
            if self.images.over.width then self.width = self.images.over.width; end
            if self.images.over.height then self.height = self.images.over.height; end
            
            self.x, self.y = self.x, self.y
        else
            self.fill = self.images.default
            self.notPressed = true

            if self.images.default.type == "composite" then self.fill.effect = eff; end
            
            if self.images.default.width then self.width = self.images.default.width; end
            if self.images.default.height then self.height = self.images.default.height; end
            
            self.x, self.y = xOver, yOver
        end

    end
    
    ---------------------------
    -- Initializes button
    function button:prepare()

        local _default
        local _over

        if type(default) == "table" then
            _default = {
               type="composite",
               paint1={ type="image", filename=default[2] },
               paint2={ type="image", filename=default[1] },
            }
        else _default = { type = "image", filename = default }; end

        if type(over) == "table" then
            _over = {
               type="composite",
               paint1={ type="image", filename=over[1] },
               paint2={ type="image", filename=over[2] },
            }
            
        else _over = { type = "image", filename = over }; end

        _default.width = width
        _default.height = height
        
        if overWidth then _over.width = overWidth; end
        if overHeight then _over.height = overHeight; end
        
        self.images = {
            default   = _default,
            over      = _over
        }
        
        -- Set the initial state
        self:toggle()
        
        -- Add the callback
        self.turnOn, self.turnOff = turnOn, turnOff
        
        -- Add the event listener
        self.touch = listener
        self:addEventListener( "touch", self )
        
        -- Set the position
        self.anchorX, self.anchorY = anchorX, anchorY
        self.x, self.y = x, y
        self.xOriginal, self.yOriginal = self.x, self.y
    end
    
    ---------------------------
    -- Call this before removing
    -- button object
    function button:cleanUp()
        self.turnOn = nil
        self.turnOff = nil
        
        self.images.default.type = nil
        self.images.default.filename = nil
        
        self.images.over.type = nil
        self.images.over.filename = nil
        
        self.images.default = nil
        self.images.over = nil
        
        self.images = nil
        
        self:removeEventListener( "touch", self )
    end
    

    listener = function( self, event )
        if  event.phase == "moved" and self.scrollView then
            local sv = self.scrollView
            local dx = math.abs( ( event.x - event.xStart ) )
            if ( dx > 10 ) then
                sv:takeFocus( event )
            end
        elseif event.phase == "ended" then
            self:toggle( 1 )    
        end
        
        return true;
    end

    button:prepare()
        
    if group then
        group:insert( button )
    end

    return button;
end
WidgetHelper.createToggleButton = createToggleButton

------------------------------------------------
-- Checks a set of toggle buttons to ensure they're
-- not all activated
--
-- @param   int             child 	Index of the active toggle button.
-- @param   DisplayGroup    group   The display group composed of toggle buttons
checkToggleButtonStates = function( child, group )

    for i=1, group.numChildren do
        if i ~= child then
            local obj = group[i]
            if not obj.notPressed then
            	obj:toggle( 1 );
            end
        end
    end
end
WidgetHelper.checkToggleButtonStates = checkToggleButtonStates

return WidgetHelper